# SisFall Temporally Annotated

Location of this file: https://bitbucket.org/unipv_cvmlab/sisfalltemporallyannotated/
If you are interested in ready-to-use training & test sets, jump to the last section.

## Introduction

This repository contains an enhancement of the [SisFall dataset](http://sistemic.udea.edu.co/en/research/projects/english-falls/), as described in:
Musci, M., De Martini, D., Blago, N., Facchinetti, T., Piastra, M.: "Online Fall Detection using Recurrent Neural Networks".

#### List of contents
This repository contains:

* The folder "Sisfall_enhanced" contains the temporally annotated dataset.
* The zip file "Sisfall_enhanced" contains the same as above, in a single file, to ease of download.
* This README file.

## SisFall_enhanced
The folder named "Temporal Annotation" has a structure identical to the original [SisFall dataset](http://sistemic.udea.edu.co/en/research/projects/english-falls/) :
Each folder represents a separate subject. Inside each of these folders there are the files corresponding to the activities.
Each of these files has the same number of rows as the original files, and a single column. The value is:

* "0" for BKG
* "1" for ALERT
* "2" for FALL

This folder is also found inside the zip file for ease of download.

## Ready-to-use sets
These sets are located here:
https://drive.google.com/drive/folders/1EHgACS56hfWVGdqP0Tp2O6cdenCoTWPt?usp=sharing

This drive contains two ready-to-use sets, for a faster implementation of other machine learning methods.
These two sets are processed from the original SisFall dataset and our temporal annotation in a similar fashion:

* Each activity has been divided into windows with length w=256 and stride s=128.
* The "x" windows contain a normalized version of original SisFall data.
* The "y" windows contain the corresponding label based on our annotation.
* The "weights" file contains the relative weight each class should have, according to its frequency (the order is: FALL, ALERT, BKG for three classes, and: FALL, BKG for two classes). BKG is always 1.
* We divided the whole dataset in three sets: train, validation, and test.

The Test set contains the entirety of five identities ("SA20", "SA21", "SA22", "SA23", "SE14", "SE15").

The remaining identities were divided with a 80%/20% random chance into the Train and Validation sets.

The Train and Validation are used during training, while the Test is used as "new data" to check the generalization power of the network.

Two different sets are provided:

* "Three Classes" uses all three classes of events (FALL, ALERT and BKG).
* "Two Classes" uses only two classes of events (FALL and BKG). Every ALERT has been converted into BKG.

Finally, the file "utils.txt" contains the shapes of every file, and two sample functions to load the data, to ease the use of the dataset.
